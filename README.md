## Project: SUN-CHASER HONG KONG (Web)
* Member: Tony (@Tony_Lai)
* Member: Woody (@Woodycck)
* Member: CHiT (@yukchit)

SUN-CHASER is a social platform for people who love hiking and taking beautiful scenery photos. User can get the latest sunrise & sunset time and the weather information so that user can make better decision on what time and which place to go. 

Besides , interactive map service is provided to let users discover , communicate & share the particular spots and sunrise/sunset photos over all 18 districts in Hong Kong.