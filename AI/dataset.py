#%%
## Import the necessary python packages

import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import os
import numpy as np
import matplotlib.pyplot as plt



#%%
## Load Data
_URL = 'https://www.dropbox.com/s/h5rm3svks40lhsv/sunrisesunset_and_nonsunrisesunset_filtered.zip?dl=1'

path_to_zip = tf.keras.utils.get_file('sunrisesunset_and_nonsunrisesunset_filtered.zip', origin=_URL, extract=True)

PATH = os.path.join(os.path.dirname(path_to_zip), 'sunrisesunset_and_nonsunrisesunset_filtered')


print('finish extracting: success')



#%%
## After extracting
train_dir = os.path.join(PATH, 'train')
validation_dir = os.path.join(PATH, 'validation')

train_sunrisesunset_dir = os.path.join(train_dir, 'sunrisesunset')  # directory with our training sunrisesunset pictures
train_nonsunrisesunset_dir = os.path.join(train_dir, 'nonsunrisesunset')  # directory with our training nonsunrisesunset pictures
validation_nonsunrisesunset_dir = os.path.join(validation_dir, 'sunrisesunset')  # directory with our validation sunrisesunset pictures
validation_sunrisesunset_dir = os.path.join(validation_dir, 'nonsunrisesunset')  # directory with our validation nonsunrisesunset pictures


#%%
## Understand the data
num_sunrisesunset_tr = len(os.listdir(train_sunrisesunset_dir))
num_nonsunrisesunset_tr = len(os.listdir(train_nonsunrisesunset_dir))

num_sunrisesunset_val = len(os.listdir(validation_sunrisesunset_dir))
num_nonsunrisesunset_val = len(os.listdir(validation_nonsunrisesunset_dir))

total_train = num_sunrisesunset_tr + num_nonsunrisesunset_tr
total_val = num_sunrisesunset_val + num_nonsunrisesunset_val


print('total training sunrise & sunset images:', num_sunrisesunset_tr)
print('total training non-sunrise & non-sunset images:', num_nonsunrisesunset_tr)

print('total validation sunrise & sunset images:', num_sunrisesunset_val)
print('total validation non-sunrise & non-sunset images:', num_nonsunrisesunset_val)
print("--")
print("Total training images:", total_train)
print("Total validation images:", total_val)


# %%
## For convenience, set up variables to use while pre-processing the dataset and training the network.
batch_size = 128
epochs = 500
IMG_HEIGHT = 160
IMG_WIDTH = 160



#%%
## Data preparation

train_image_generator = ImageDataGenerator(rescale=1./255) # Generator for our training data
validation_image_generator = ImageDataGenerator(rescale=1./255) # Generator for our validation data

train_data_gen = train_image_generator.flow_from_directory(batch_size=batch_size,
                                                           directory=train_dir,
                                                           shuffle=True,
                                                           target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                           class_mode='binary')

val_data_gen = validation_image_generator.flow_from_directory(batch_size=batch_size,
                                                              directory=validation_dir,
                                                              target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                              class_mode='binary')

#%%
## Visualize training images

sample_training_images, _ = next(train_data_gen)

# This function will plot images in the form of a grid with 1 row and 5 columns where images are placed in each column.
def plotImages(images_arr):
    fig, axes = plt.subplots(1, 5, figsize=(20,20))
    axes = axes.flatten()
    for img, ax in zip( images_arr, axes):
        ax.imshow(img)
        ax.axis('off')
    plt.tight_layout()
    plt.show()

plotImages(sample_training_images[:5])



# %%
## A Better Data preparation

image_gen_train = ImageDataGenerator(
                    rescale=1./255,
                    rotation_range=45,
                    width_shift_range=.15,
                    height_shift_range=.15,
                    horizontal_flip=True,
                    zoom_range=0.5
                    )

train_data_gen = image_gen_train.flow_from_directory(batch_size=batch_size,
                                                     directory=train_dir,
                                                     shuffle=True,
                                                     target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                     class_mode='binary')


#%%
## Visualize new training images
augmented_images = [train_data_gen[0][0][0] for i in range(5)]
plotImages(augmented_images)

augmented_images = [train_data_gen[0][0][1] for i in range(5)]
plotImages(augmented_images)

augmented_images = [train_data_gen[0][0][15] for i in range(5)]
plotImages(augmented_images)
# print(augmented_images)


# %%
## New validation data generator
image_gen_val = ImageDataGenerator(rescale=1./255)

val_data_gen = image_gen_val.flow_from_directory(batch_size=batch_size,
                                                 directory=validation_dir,
                                                 target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                 class_mode='binary')



# %%
