#%%

# First install https://github.com/yakupadakli/python-unsplash
# Unsplash API https://unsplash.com/documentation
import json
import os
import urllib.request
from unsplash.api import Api
from unsplash.auth import Auth
with open('tokens.json', 'r') as f:
    data = json.load(f)
client_id = data['client_id']
client_secret = data['client_secret']
redirect_uri = ""
code = ""


#%%
## Downloading Training Photos of Sunrise & Sunset
keyword = 'sunrise sunset'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 0
for i in range(1, 35):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:
        item = {"id" : photo.id, "filename" : "sunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "sunrisesunset" + "{}".format(x) + ".jpg"
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/sunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('sunrisesunset_training.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download training photos of sunrise & sunset: success")



# %%
## Downloading Validation Photos of Sunrise & Sunset
keyword = 'sunrise sunset'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 2000
for i in range(37, 58):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:
        item = {"id" : photo.id, "filename" : "sunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "sunrisesunset" + "{}".format(x) + ".jpg"
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/validation/sunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('sunrisesunset_validation.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download validation photos of sunrise & sunset: success")



#%%
## Downloading Training Photos of City (for data of NonSunriseSunset)
keyword = 'city'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 0
for i in range(1, 12):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('city_training.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download training photos of city: success")




#%%
## Downloading Training Photos of Animal (for data of NonSunriseSunset)
import os

keyword = 'animal'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 330
for i in range(1, 6):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:   
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('animal_training.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download training photos of animal: success")


#%%
## Downloading Training Photos of Hong Kong (for data of NonSunriseSunset)
import os

keyword = 'hong kong'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 690
for i in range(1, 16):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:   
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('hongkong_training.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download training photos of hong kong: success")



#%%
## Downloading Training Photos of Hong Kong Landscape (for data of NonSunriseSunset)
import os

keyword = 'hong kong landscape'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 1140
for i in range(1, 4):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:   
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('hongkong_training.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download training photos of hong kong's landscape: success")


#%%
## Downloading Training Photos of Landscape (for data of NonSunriseSunset)
import os

keyword = 'landscape'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 1229
for i in range(1, 4):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:   
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('landscape_training.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download training photos of landscape: success")



# %%
## Downloading Validation Photos of City  (for data of NonSunriseSunset)
keyword = 'city'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 2000
for i in range(13, 16):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/validation/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('sunrisesunset_validation.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download validation photos of city: success")



# %%
## Downloading Validation Photos of Hong Kong's Landscape (for data of NonSunriseSunset)
keyword = 'hong kong landscape'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 2090
for i in range(5, 10):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/validation/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('sunrisesunset_validation.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download validation photos of hong kong's landscape: success")



# %%
## Downloading Validation Photos of Pets (for data of NonSunriseSunset)
keyword = 'pet'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 2240
for i in range(1, 3):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/validation/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('sunrisesunset_validation.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download validation photos of pets: success")



# %%
## Downloading Validation Photos of Landscape (for data of NonSunriseSunset)
keyword = 'landscape'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 2300
for i in range(5, 10):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/validation/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('sunrisesunset_validation.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download validation photos of landscape: success")



# %%
## Downloading Validation Photos of Hong Kong (for data of NonSunriseSunset)
keyword = 'hong kong'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 2450
for i in range(23, 27):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/validation/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('sunrisesunset_validation.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download validation photos of hong kong: success")



#%%
## Downloading >500 More Training Photos of Sunset
keyword = 'selfie sunset'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 560
for i in range(3, 5):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:
        item = {"id" : photo.id, "filename" : "sunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "sunrisesunset" + "{}".format(x) + ".jpg"
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/sunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('sunrisesunset_training.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download >500 more training photos of hong kong's sunset: success")



#%%
## Downloading >500 Training Photos for data of NonSunriseSunset
import os

keyword = 'fire'
auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)
dataOfPhotos = []
x = 10541
for i in range(1, 3):
    photos = api.search.photos(keyword, per_page=30, page=i)["results"]
    for photo in photos:   
        print(photo)
        print(photo.id)
        # print(photo.urls)
        print(photo.urls.small)
        full_name = "nonsunrisesunset" + "{}".format(x) + ".jpg"
        item = {"id" : photo.id, "filename" : "nonsunrisesunset" + str(x) + ".jpg", "url" : photo.urls.small}
        x += 1
        path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/nonsunrisesunset/'
        local_filename, headers = urllib.request.urlretrieve(photo.urls.small, path + full_name)
        dataOfPhotos.append(item)
# jsonData=json.dumps(dataOfPhotos)
# print(jsonData)
# with open('landscape_training.txt', 'w') as outfile:
#     json.dump(dataOfPhotos, outfile)
print("download >500 more training photos of non-sunrise & non-sunset: success")





#%%
## Renaming all the training photos in nonsunrisesunset folder from 0 to 1499
import glob
import os

path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/nonsunrisesunset/'
i = 0
for i, filename in enumerate(glob.glob(path + '*.jpg')):
    os.rename(filename, os.path.join(path, 'nonsunrisesunset.' + str(i) + '.jpg'))
    i = i + 1
print("Renaming all the training photos in nonsunrisesunset folder: success")



#%%
## Renaming all the training photos in sunrisesunset folder from 0 to 1499
import glob
import os

path = 'sunrisesunset_and_nonsunrisesunset_filtered/train/sunrisesunset/'
i = 0
for i, filename in enumerate(glob.glob(path + '*.jpg')):
    os.rename(filename, os.path.join(path, 'sunrisesunset.' + str(i) + '.jpg'))
    i = i + 1
print("Renaming all the training photos in sunrisesunset folder: success")



#%%
## Renaming all the validation photos in nonsunrisesunset folder from 2000 to 2499
import glob
import os

path = 'sunrisesunset_and_nonsunrisesunset_filtered/validation/nonsunrisesunset/'
i = 0
for i, filename in enumerate(glob.glob(path + '*.jpg')):
    os.rename(filename, os.path.join(path, 'nonsunrisesunset.' + str(2000+i) + '.jpg'))
    i = i + 1
print("Renaming all the validation photos in nonsunrisesunset folder: success")



#%%
## Renaming all the validation photos in sunrisesunset folder from 2000 to 2499
import glob
import os

path = 'sunrisesunset_and_nonsunrisesunset_filtered/validation/sunrisesunset/'
i = 0
for i, filename in enumerate(glob.glob(path + '*.jpg')):
    os.rename(filename, os.path.join(path, 'sunrisesunset.' + str(2000+i) + '.jpg'))
    i = i + 1
print("Renaming all the validation photos in sunrisesunset folder: success")



# %%
