#%%
from keras.models import load_model
from keras.preprocessing import image
from flask import Flask, request
import tensorflow as tf
import numpy as np
import json

print(tf.__version__)



# dimensions of our images
img_width, img_height = 160, 160

# load the model we saved
model = load_model('saved_model_500epoch.h5')
# model.compile(loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
#               optimizer='adam',
#               metrics=['accuracy'])




#%%
# predicting images
# img = image.load_img('samples/yes03.jpg', target_size=(img_width, img_height))
# print(img)
# x = image.img_to_array(img)
# print(x)
# x = np.expand_dims(x, axis=0)

# x /= 255.

# x = np.array([x])

# (1, 150, 150, 3)
# images = np.vstack([x])
# print(x.shape)

# WARNING:tensorflow:From index.py:34: Sequential.predict_classes (from tensorflow.python.keras.engine.sequential) is deprecated and will be removed after 2021-01-01.
# classes = model.predict_classes(x, batch_size=1)
# print(classes[0][0])

# classes = (model.predict(x) > 0.5).astype("int32")
# print(classes[0][0])

# if classes[0][0] == 0:
#     print("Oh.. A.I. guesses this is NOT sunrise NOR sunset")
# else:
#     print("BINGO! A.I. guesses this is sunrise or sunset")





#%%

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = './'

@app.route('/', methods=['POST'])
def index():
    print(request)
    print(request.files)
    f = request.files['the_photo']
    if f:
        # return "You uploaded " + f.filename
        img = image.load_img("../public/uploads/" + f.filename, target_size=(img_width, img_height))
        x = image.img_to_array(img)
        x /= 255.
        x = np.array([x])
        classes = (model.predict(x) > 0.5).astype("int32")
        if classes[0][0] == 0:
            return json.dumps({"sunriseOrSunset": "false"})
        else:
            return json.dumps({"sunriseOrSunset": "true"})

    else:
        return "No file uploaded"

app.run()



# %%
