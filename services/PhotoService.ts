import Knex from "knex";

export class PhotoService {
  constructor(private knex: Knex) {}

  async getPhotosByUpdatedAt(
    page: number,
    userId: number | undefined = undefined
  ) {
    // select image, username, photos.updated_at,photos.id,title,description,district,location from photos join users on users.id = photos.user_id
    const createQuery = () =>
      this.knex("photos")
        .join("users", "photos.user_id", "users.id")
        .leftJoin("likes", "likes.photo_id", "photos.id");
    const result = await createQuery().count("*").first();
    const photoQuery = createQuery()
      .column("photos.*", "users.username")
      .count("likes.id as total_likes")
      .groupBy("photos.id", "users.username")
      .select()
      .whereNot("photos.status", "disapproved")
      .orderBy("updated_at", "desc")
      .limit(10);
    if (userId) {
      photoQuery.select(
        this.knex.raw(
          "count(likes.id) FILTER (WHERE likes.user_id = ?) ::INTEGER as liked",
          [userId]
        )
      );
    }
    const photos = await photoQuery;
    return {
      total: parseInt(result?.count as string),
      photos,
    };
  }

  async getPhotosTotalComments() {
    // select photos.id, count(comments.id) as total_comments from photos
    // join comments on comments.photo_id = photos.id
    // group by photos.id

    const comments = await this.knex
      .column("photos.id", "photos.updated_at", "photos.status")
      .select()
      .count("comments.id as total_comments")
      .from("photos")
      .leftJoin("comments", "photos.id", "comments.photo_id")
      .groupBy("photos.id")
      .orderBy("updated_at", "desc")
      .whereNot("photos.status", "disapproved")
      .limit(10);
    return comments;
  }

  async getMyPhotosTotalComments(userId: number) {
    // const comments = await this.knex.raw(
    //   'select "photos"."id", "photos"."updated_at", "photos"."status", count("comments"."id") as "total_comment" from "photos" inner join "users" on "photos"."user_id" = "users"."id"  left join "comments" on "comments"."photo_id" = "photos"."id"  group by "photos"."id", "photos"."updated_at", "photos"."status", "users"."id" having  "photos"."user_id" = "users"."id" '
    // )

    const comments = await this.knex
      .column("photos.id", "photos.updated_at", "photos.status")
      .select()
      .count("comments.id as total_comments")
      .from("photos")
      .join("users", "photos.user_id", "users.id")
      .leftJoin("comments", "photos.id", "comments.photo_id")
      .groupBy("photos.id")
      .orderBy("updated_at", "desc")
      .where("photos.user_id", userId)
      .whereNot("photos.status", "disapproved");

    return comments;
  }

  async getDistrictPhotosTotalComments(district: string) {
    const comments = await this.knex
      .column(
        "photos.id",
        "photos.updated_at",
        "photos.status",
        "photos.district"
      )
      .select()
      .count("comments.id as total_comments")
      .from("photos")
      .leftJoin("comments", "photos.id", "comments.photo_id")
      .groupBy("photos.id")
      .orderBy("updated_at", "desc")
      .where("photos.district", district)
      .whereNot("photos.status", "disapproved");

    return comments;
  }

  async getPhotosByUserId(userId: number, currentUserId: number | undefined) {
    // select  image, username, photos.updated_at,photos.id,title,description,district,location from photos join users on users.id = photos.user_id
    // where username = 'God Woody'
    // order by updated_at DESC

    const createQuery = () =>
      this.knex("photos")
        .join("users", "photos.user_id", "users.id")
        .leftJoin("likes", "likes.photo_id", "photos.id");

    const photoQuery = createQuery()
      .column("photos.*", "users.username")
      .select()
      .count("likes.id as total_likes")
      .groupBy("photos.id", "users.username")
      .where("photos.user_id", userId)
      .whereNot("photos.status", "disapproved")
      .orderBy("updated_at", "desc");

    if (currentUserId) {
      console.log("userId", currentUserId);
      photoQuery.select(
        this.knex.raw(
          "count(likes.id) FILTER (WHERE likes.user_id = ?) ::INTEGER as liked",
          [userId]
        )
      );
    }
    const photos = await photoQuery;
    return photos;
  }

  async getPhotosByDistrict(
    district: string,
    userId: number | undefined = undefined
  ) {
    // select  image, username, photos.updated_at,photos.id,title,description,photos.district,location from photos join users on users.id = photos.user_id
    // where photos.district = 'Tsuen Wan'
    // order by updated_at DESC

    const createQuery = () =>
      this.knex("photos")
        .join("users", "photos.user_id", "users.id")
        .leftJoin("likes", "likes.photo_id", "photos.id");

    const photoQuery = createQuery()
      .column("photos.*", "users.username")
      .select()
      .count("likes.id as total_likes")
      .groupBy("photos.id", "users.username")
      .where("photos.district", district)
      .whereNot("photos.status", "disapproved")
      .orderBy("updated_at", "desc");

    if (userId) {
      console.log("userId", userId);
      photoQuery.select(
        this.knex.raw(
          "count(likes.id) FILTER (WHERE likes.user_id = ?) ::INTEGER as liked",
          [userId]
        )
      );
    }
    const photos = await photoQuery;
    return photos;
  }

  async getPhotosByComments() {
    // select  photos.image, users.username, photos.updated_at,photos.id ,count(comments.id) as total_comment, title,description,district,location from photos join users on users.id = photos.user_id
    // left join comments on comments.photo_id = photos.id
    // group by photos.image, users.username, photos.updated_at,photos.id, title,description,district,location
    //    order by total_comment DESC
    const photos = await this.knex
      .column(
        "users.username",
        "photos.id",
        "photos.image",
        "photos.title",
        "photos.description",
        "photos.district",
        "photos.location",
        "photos.updated_at",
        "photos.user_id",
        "photos.status",
        "photos.environment"
      )
      .count("comments.id as total_comment")
      .select()
      .from("photos")
      .join("users", "photos.user_id", "users.id")
      .leftJoin("comments", "comments.photo_id", "photos.id")
      .groupBy(
        "users.username",
        "photos.id",
        "photos.image",
        "photos.title",
        "photos.description",
        "photos.district",
        "photos.location",
        "photos.updated_at",
        "photos.user_id",
        "photos.status",
        "photos.environment"
      )
      .orderBy("total_comment", "desc");

    return photos;
  }

  async getPhotosByLikes() {
    // select  photos.image, users.username, photos.updated_at,photos.id ,count(likes.id) as total_like, title,description,district,location from photos join users on users.id = photos.user_id
    // left join likes on likes.photo_id = photos.id
    // group by photos.image, users.username, photos.updated_at,photos.id, title,description,district,location
    //   order by total_like DESC

    const photos = await this.knex
      .column(
        "users.username",
        "photos.id",
        "photos.image",
        "photos.title",
        "photos.description",
        "photos.district",
        "photos.location",
        "photos.updated_at",
        "photos.user_id",
        "photos.status",
        "photos.environment"
      )
      .count("likes.id as total_like")
      .select()
      .from("photos")
      .join("users", "photos.user_id", "users.id")
      .leftJoin("likes", "likes.photo_id", "photos.id")
      .groupBy(
        "users.username",
        "photos.id",
        "photos.image",
        "photos.title",
        "photos.description",
        "photos.district",
        "photos.location",
        "photos.updated_at",
        "photos.user_id",
        "photos.status",
        "photos.environment"
      )
      .orderBy("total_like", "desc");

    return photos;
  }

  async getCommentsByPhotoId(photoId: number) {
    const result = this.knex
      .column("comments.*", "users.username")
      .select()
      .from("comments")
      .join("users", "comments.user_id", "users.id")
      .where("photo_id", photoId)
      .orderBy("created_at", "asc");
    return result;
  }

  async getLikedPhotos(userId: number) {
    const result = await this.knex
      .column("photos.id", "likes.user_id")
      .select()
      .from("photos")
      .join("likes", "likes.photo_id", "photos.id")
      .where("likes.user_id", userId);
    return result;
  }

  // ts by Woody, revised by CHiT
  async addPhotos(
    body: {
      photo_title: string;
      photo_description: string;
      photo_created_at: Date;
      photo_district: string;
      photo_location: string;
      photo_latitude: number;
      photo_longitude: number;
      photo_userId: number;
      photo_environment: string;
      photo_status: string;
    },
    filename: string
  ) {
    const [photoId] = await this.knex("photos")
      .insert({
        title: body.photo_title,
        description: body.photo_description,
        district: body.photo_district,
        created_at: body.photo_created_at,
        location: body.photo_location,
        latitude: body.photo_latitude,
        longitude: body.photo_longitude,
        user_id: body.photo_userId,
        image: filename,
        environment: body.photo_environment,
        status: body.photo_status,
      })
      .returning("id");
    return photoId as number;
  }

  //ts by CHiT
  async getPendingAndDisapprovedPhotos() {
    // select photos.id, photos.image, photos.title, photos.description, photos.location, photos.district, photos.created_at, photos.updated_at, photos.latitude, photos.longitude, photos.status, photos.environment, users.username from photos left join users on photos.user_id = users.id where photos.status in ('pending', 'disapproved') order by updated_at desc;
    const pendingAndDisapprovedPhotos = await this.knex
      .select(
        "photos.id",
        "photos.image",
        "photos.title",
        "photos.description",
        "photos.location",
        "photos.district",
        "photos.created_at",
        "photos.updated_at",
        "photos.latitude",
        "photos.longitude",
        "photos.status",
        "photos.environment",
        "users.username"
      )
      .from("photos")
      .leftJoin("users", "photos.user_id", "users.id")
      .whereIn("photos.status", ["pending", "disapproved"])
      .orderBy("updated_at", "desc");

    return pendingAndDisapprovedPhotos;
  }

  async updatePhotoStatusRN(photoStatus: string, photoId: number) {
    // update photos set status = 'shown' where id = 39 returning "id", "status";
    const PhotoStatusRN = await this.knex("photos")
      .where({ id: photoId })
      .update({ status: photoStatus }, ["id", "status"]);

    return PhotoStatusRN[0];
  }

  //ts by Woody
  async updatePhoto(title: string, photoId: number) {
    await this.knex("photos").update("title", title).where("id", photoId);
  }

  async deletePhoto(photoId: number) {
    await this.knex("comments").delete().where("photo_id", photoId);
    await this.knex("likes").delete().where("photo_id", photoId);
    await this.knex("photos").delete().where("id", photoId);
  }
}
